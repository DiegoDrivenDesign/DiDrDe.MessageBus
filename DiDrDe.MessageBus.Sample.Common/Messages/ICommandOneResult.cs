﻿using System;
using DiDrDe.MessageBus.Messages;

namespace DiDrDe.MessageBus.Sample.Common.Messages
{
    public interface ICommandOneResult
        : IMessage
    {
        DateTime CreatedOn { get; }
    }
}