﻿using System;
using DiDrDe.MessageBus.Messages;

namespace DiDrDe.MessageBus.Sample.Common.Messages
{
    public interface IEventOne
        : IEvent
    {
        Guid Id { get; }
        DateTime CreatedOn { get; }
    }
}