﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts;
using FluentAssertions;
using MassTransit;
using System;
using System.Threading.Tasks;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.EventBusTests
{
    public static class PublishAsyncTests
    {
        public class Given_An_ActiveMq_EventBus_When_Publish
            : Given_WhenAsync_Then_Test
        {
            private IEventBus _sut;
            private IFakeEvent _eventToSend;
            private Exception _handledException;

            protected override void Given()
            {
                const string endpointName = "fakeEventEndpoint";

                var builder = new ContainerBuilder();

                builder
                    .RegisterActiveMqEventPublisher(
                        context =>
                        {
                            var messageBusOptions =
                                new ActiveMqOptions
                                {
                                    HostName = ActiveMqTestsConstants.HostName,
                                    EndpointName = endpointName,
                                    Port = ActiveMqTestsConstants.Port,
                                    Username = ActiveMqTestsConstants.Username,
                                    Password = ActiveMqTestsConstants.Password,
                                    UseSsl = ActiveMqTestsConstants.UseSsl,
                                    AutoDelete = ActiveMqTestsConstants.AutoDelete
                                };
                            return messageBusOptions;
                        });

                var container = builder.Build();
                var componentContext = container.Resolve<IComponentContext>();
                var publishEndpoint = componentContext.Resolve<IPublishEndpoint>();
                _sut = new EventBus(publishEndpoint);
                var id = GuidGenerator.Create(1);
                _eventToSend = new FakeEvent(id);
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    await _sut.Publish(_eventToSend);
                }
                catch (Exception exception)
                {
                    _handledException = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Raise_Any_Exception()
            {
                _handledException.Should().BeNull();
            }
        }
    }
}