﻿using DiDrDe.MessageBus.Messages;
using System;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts
{
    public interface IFakeEvent
        : IEvent
    {
        Guid Id { get; }
    }
}