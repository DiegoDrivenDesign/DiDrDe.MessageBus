﻿using DiDrDe.MessageBus.Messages;
using System;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts
{
    public interface IFakeCommand
        : ICommand
    {
        Guid Id { get; }
    }
}