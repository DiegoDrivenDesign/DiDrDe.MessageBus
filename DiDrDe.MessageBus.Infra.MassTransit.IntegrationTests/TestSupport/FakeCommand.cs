﻿using System;
using DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport.Contracts;

namespace DiDrDe.MessageBus.Infra.MassTransit.IntegrationTests.TestSupport
{
    public class FakeCommand
        : IFakeCommand
    {
        public Guid Id { get; }

        public FakeCommand(Guid id)
        {
            Id = id;
        }
    }
}