﻿using DiDrDe.MessageBus.Infra.MassTransit.Autofac.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.Contracts;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using FluentAssertions;
using MassTransit;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac.UnitTests.BusControls.BusControlWrapperTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private BusControlWrapper _sut;
            private IBusControl _busControl;

            protected override void Given()
            {
                _busControl = Mock.Of<IBusControl>();
            }

            protected override void When()
            {
                _sut = new BusControlWrapper(_busControl);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IBusControlWrapper()
            {
                _sut.Should().BeAssignableTo<IBusControlWrapper>();
            }

            [Fact]
            public void Then_It_Should_Be_An_IMessageBusControl()
            {
                _sut.Should().BeAssignableTo<IMessageBusControl>();
            }

            [Fact]
            public void Then_It_Should_Be_An_IBusControl()
            {
                _sut.Should().BeAssignableTo<IBusControl>();
            }

            [Fact]
            public void Then_It_Should_Be_An_ICommandConsumerMessageBusControl()
            {
                _sut.Should().BeAssignableTo<ICommandConsumerMessageBusControl>();
            }

            [Fact]
            public void Then_It_Should_Be_An_ICommandSenderMessageBusControl()
            {
                _sut.Should().BeAssignableTo<ICommandSenderMessageBusControl>();
            }

            [Fact]
            public void Then_It_Should_Be_An_IEventConsumerMessageBusControl()
            {
                _sut.Should().BeAssignableTo<IEventConsumerMessageBusControl>();
            }
        }
    }
}