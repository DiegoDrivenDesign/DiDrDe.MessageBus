﻿using System.Threading;
using System.Threading.Tasks;
using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport;
using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts;
using MassTransit;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.EventBusTests
{
    public static class PublishTests
    {
        public class Given_An_EventBus_When_Publishing_Async
            : Given_WhenAsync_Then_Test
        {
            private EventBus _sut;
            private Mock<IPublishEndpoint> _publishEndpointMock;
            private IFakeEvent _event;

            protected override void Given()
            {
                _publishEndpointMock = new Mock<IPublishEndpoint>();
                _event = new FakeEvent();
                var publishEndpoint = _publishEndpointMock.Object;
                _sut = new EventBus(publishEndpoint);
            }

            protected override async Task WhenAsync()
            {
                await _sut.Publish(_event);
            }

            [Fact]
            public void Then_It_Should_Use_Publish_Endpoint_To_Publish_The_Event()
            {
                _publishEndpointMock.Verify(x => x.Publish(_event, It.IsAny<CancellationToken>()), Times.Once);
            }
        }
    }
}