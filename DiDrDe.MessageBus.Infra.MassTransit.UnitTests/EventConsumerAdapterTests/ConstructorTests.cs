﻿using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport;
using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts;
using FluentAssertions;
using MassTransit;
using Moq;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.EventConsumerAdapterTests
{
    public static class ConstructorTests
    {
        public class Given_Valid_Dependencies_When_Constructing_Instance
            : Given_When_Then_Test
        {
            private EventConsumerAdapter<IFakeEvent> _sut;
            private IEventConsumer<IFakeEvent> _eventConsumer;

            protected override void Given()
            {
                _eventConsumer = Mock.Of<IEventConsumer<IFakeEvent>>();
            }

            protected override void When()
            {
                _sut = new EventConsumerAdapter<IFakeEvent>(_eventConsumer);
            }

            [Fact]
            public void Then_It_Should_Have_Created_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_IConsumer_Of_The_Event()
            {
                _sut.Should().BeAssignableTo<IConsumer<FakeEvent>>();
            }
        }
    }
}