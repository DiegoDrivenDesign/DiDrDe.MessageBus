﻿using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.UnitTests.TestSupport.Contracts;
using FluentAssertions;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.UnitTests.Configuration.CommandTypesOptionsTests
{
    public static class ConsumesEventTests
    {
        public class Given_An_CommandTypesOptions_When_SendingCommand
            : Given_When_Then_Test
        {
            private int _expectedLenght;
            private CommandTypesOptions _sut;

            protected override void Given()
            {
                _expectedLenght = 1;
                _sut = new CommandTypesOptions();
            }

            protected override void When()
            {
                _sut.SendsCommand<IFakeCommand, IFakeResponse>();
            }

            [Fact]
            public void Then_It_Should_Have_One_Item_In_The_CommandTypes_Property()
            {
                _sut.CommandTypes.Count.Should().Be(_expectedLenght);
            }
        }
    }
}