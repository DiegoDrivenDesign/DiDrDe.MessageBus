﻿using DiDrDe.MessageBus.Messages;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus
{
    public interface IQueryBus
    {
        Task<TQueryResponse> Send<TQuery, TQueryResponse>(TQuery request)
            where TQuery : class, IQuery
            where TQueryResponse : class, IMessage;
    }
}