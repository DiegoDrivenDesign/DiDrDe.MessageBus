﻿using DiDrDe.MessageBus.Messages;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus
{
    public interface ICommandBus
    {
        Task<TCommandResponse> Send<TCommand, TCommandResponse>(TCommand command)
            where TCommand : class, ICommand
            where TCommandResponse : class, IMessage;
    }
}