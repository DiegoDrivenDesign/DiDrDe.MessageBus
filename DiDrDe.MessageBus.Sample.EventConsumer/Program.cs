﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using DiDrDe.MessageBus.Sample.Common;
using DiDrDe.MessageBus.Sample.Common.Messages;
using MassTransit.Util;
using System;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus.Sample.EventConsumer
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Event consumer sample");
            var containerBuilder = new ContainerBuilder();

            containerBuilder
                .RegisterType<EventOneConsumer>()
                .As<IEventConsumer<IEventOne>>();

            containerBuilder
                .RegisterActiveMqEventConsumer(
                    eventOptions =>
                    {
                        eventOptions.ConsumesEvent<IEventOne>();
                    },
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = Constants.ActiveMqConstants.HostName,
                                Port = Constants.ActiveMqConstants.Port,
                                EndpointName = "test_queue_event_consumer",
                                Username = Constants.ActiveMqConstants.Username,
                                Password = Constants.ActiveMqConstants.Password,
                                UseSsl = Constants.ActiveMqConstants.UseSsl,
                                AutoDelete = Constants.ActiveMqConstants.AutoDelete
                            };
                        return messageBusOptions;
                    });

            var container = containerBuilder.Build();
            var componentContext = container.Resolve<IComponentContext>();
            var busManager = componentContext.Resolve<IEventConsumerMessageBusManager>();
            TaskUtil.Await(() => busManager.Start());
            Console.WriteLine("Press any key to exit");
            Console.In.ReadLine();
            TaskUtil.Await(() => busManager.Stop());
        }
    }

    public class EventOneConsumer
        : IEventConsumer<IEventOne>
    {
        public Task Consume(IEventOne eventOne)
        {
            var id = eventOne.Id;
            var createdOn = eventOne.CreatedOn;
            Console.WriteLine($"Received {eventOne} on {createdOn} with id: {id}");
            return Task.CompletedTask;
        }
    }
}