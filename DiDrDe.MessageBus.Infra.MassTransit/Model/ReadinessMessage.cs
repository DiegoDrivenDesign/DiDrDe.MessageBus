﻿using DiDrDe.MessageBus.Messages;

namespace DiDrDe.MessageBus.Infra.MassTransit.Model
{
    public class ReadinessMessage
        : IMessage
    {
        public string Text { get; }

        public ReadinessMessage(string text)
        {
            Text = text;
        }
    }
}