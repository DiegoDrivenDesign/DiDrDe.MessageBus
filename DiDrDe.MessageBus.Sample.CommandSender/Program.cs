﻿using Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using DiDrDe.MessageBus.Sample.Common;
using DiDrDe.MessageBus.Sample.Common.Messages;
using MassTransit.Util;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DiDrDe.MessageBus.Sample.CommandSender
{
    public class Program
    {
        static void Main()
        {
            Console.WriteLine("Command sender sample");
            var containerBuilder = new ContainerBuilder();
            containerBuilder
                .RegisterActiveMqCommandSender(
                    commandOptions =>
                    {
                        commandOptions.SendsCommand<ICommandOne, ICommandOneResult>();
                    },
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = Constants.ActiveMqConstants.HostName,
                                EndpointName = "test_queue_commands",
                                Port = Constants.ActiveMqConstants.Port,
                                Username = Constants.ActiveMqConstants.Username,
                                Password = Constants.ActiveMqConstants.Password,
                                UseSsl = Constants.ActiveMqConstants.UseSsl,
                                AutoDelete = Constants.ActiveMqConstants.AutoDelete
                            };
                        return messageBusOptions;
                    });

            var container = containerBuilder.Build();
            var componentContext = container.Resolve<IComponentContext>();
            var busManager = componentContext.Resolve<ICommandSenderMessageBusManager>();
            TaskUtil.Await(() => busManager.Start());
            TaskUtil.Await(() => SendCommands(container, 10, 200));
            Console.WriteLine("Press any key to exit");
            Console.In.ReadLine();
            TaskUtil.Await(() => busManager.Stop());
        }

        private static async Task SendCommands(IComponentContext componentContext, int numberCommands, int gapMilliSeconds)
        {
            var commandBus = componentContext.Resolve<ICommandBus>();
            for (int index = 0; index < numberCommands; index++)
            {
                Thread.Sleep(gapMilliSeconds);
                var command = new CommandOne($"Command number {index}");
                Console.WriteLine($"Sending {command}");
                var result = await commandBus.Send<ICommandOne, ICommandOneResult>(command);
                Console.WriteLine($"Result received on {result.CreatedOn}");
            }
        }
    }
}