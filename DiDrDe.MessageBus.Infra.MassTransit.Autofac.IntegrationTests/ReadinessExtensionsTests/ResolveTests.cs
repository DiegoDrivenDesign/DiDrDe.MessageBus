﻿using Autofac;
using DiDrDe.Health;
using DiDrDe.MessageBus.Infra.MassTransit.Autofac.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.BusManagers;
using DiDrDe.MessageBus.Infra.MassTransit.Configuration;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusControls;
using DiDrDe.MessageBus.Infra.MassTransit.Contracts.BusManagers;
using FluentAssertions;
using MassTransit;
using ToolBelt.TestSupport;
using Xunit;

namespace DiDrDe.MessageBus.Infra.MassTransit.Autofac.IntegrationTests.ReadinessExtensionsTests
{
    public static class ResolveTests
    {
        public class Given_A_Container_And_A_Registration_Of_ActiveMq_Readiness_Checker_When_Resolving_An_IReadinessMessageBusControl
            : Given_When_Then_Test
        {
            private IComponentContext _componentContext;
            private IReadinessMessageBusControl _sut;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder.RegisterActiveMqReadinessChecker(
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = "foo",
                                Port = 12345,
                                EndpointName = "foo",
                                Username = "foo",
                                Password = "foo",
                                UseSsl = false,
                                AutoDelete = true
                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IReadinessMessageBusControl>();
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_BusControlWrapper()
            {
                _sut.Should().BeAssignableTo<BusControlWrapper>();
            }

            [Fact]
            public void Then_It_Should_Be_An_IBus()
            {
                _sut.Should().BeAssignableTo<IBus>();
            }
        }

        public class Given_A_Container_And_A_Registration_Of_ActiveMq_Readiness_Checker_When_Resolving_An_IReadinessMessageBusManager
            : Given_When_Then_Test
        {
            private IComponentContext _componentContext;
            private IReadinessMessageBusManager _sut;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder.RegisterActiveMqReadinessChecker(
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = "foo",
                                Port = 12345,
                                EndpointName = "foo",
                                Username = "foo",
                                Password = "foo",
                                UseSsl = false,
                                AutoDelete = true
                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IReadinessMessageBusManager>();
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_MessageBusManager_Of_Its_BusControl()
            {
                _sut.Should().BeAssignableTo<MessageBusManager<IReadinessMessageBusControl>>();
            }
        }

        public class Given_A_Container_And_A_Registration_Of_ActiveMq_Readiness_Checker_When_Resolving_An_IReadiness
            : Given_When_Then_Test
        {
            private IComponentContext _componentContext;
            private IReadiness _sut;

            protected override void Given()
            {
                var builder = new ContainerBuilder();

                builder.RegisterActiveMqReadinessChecker(
                    context =>
                    {
                        var messageBusOptions =
                            new ActiveMqOptions
                            {
                                HostName = "foo",
                                Port = 12345,
                                EndpointName = "foo",
                                Username = "foo",
                                Password = "foo",
                                UseSsl = false,
                                AutoDelete = true
                            };
                        return messageBusOptions;
                    });

                var container = builder.Build();
                _componentContext = container.Resolve<IComponentContext>();
            }

            protected override void When()
            {
                _sut = _componentContext.Resolve<IReadiness>();
            }

            [Fact]
            public void Then_It_Should_Return_A_Valid_Instance()
            {
                _sut.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_A_MessageBusManager_Of_Its_BusControl()
            {
                _sut.Should().BeAssignableTo<MessageBusManager<IReadinessMessageBusControl>>();
            }
        }
    }
}